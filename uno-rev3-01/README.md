# 아두이노 우노 REV3 보드

* 보드 구입

ref)

https://store.arduino.cc/products/arduino-uno-rev3

https://store.arduino.cc/products/arduino-uno-rev3-smd

https://m.blog.naver.com/eduino/222059264567

https://m.blog.naver.com/bpcode/221812326102

https://www.twinschip.com/Arduino-UNO-R3-SMD

* 보드 설명

호환보드를 구입한 경우 DIP 타입과 SMD 타입에 따라 달라지지만 대부분 저렴한 SMD 타입인 경우가 많으므로
해당 보드에 따른 정확한 pinout을 찾기가 어려우므로 해당 보드에 맞는 사진을 찾아서 잘 보관할 것!

+ 호환 보드(SMD 타입)

![uno-rev3](./images/uno-rev3-0000.png)

![uno-rev3](./images/uno-rev3-0001.png)

![uno-rev3](./images/uno-rev3-0002.png)

NOTE. 우노 정품과 달리 핀맵이라든지 LED 위치가 다름. 그리고 가장 큰 차이는 USB 통신용 칩을 대부분 저가형 CH340칩을 이용하므로
PC에 연결에 적절한 드라이버를 찾아서 설치해 줘야하는 귀찮은 작업이 필요함.

* 정품 보드(SMD 타입)

![uno-rev3](./images/uno-rev3-0003.png)

![uno-rev3](./images/uno-rev3-0004.png)


* blink 예제

blink 예제
```
int ledPin = 13;

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
  digitalWrite(ledPin, HIGH);
  delay(1000);
  digitalWrite(ledPin, LOW);
  delay(1000);
}
```


___
.END OF UNO_REV3
