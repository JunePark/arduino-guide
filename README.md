# arduino guide

아두이노 간단 사용법 정리

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

# 설명 링크

* 아두이노 마이크로 프로
    - [01. 보드 설명](pro-micro-01/README.md)

* 아두이노 마이크로

* 아두이노 우노 R3
  - [01. 보드 설명](uno-rev3-01/README.md)
  - [02. 스위치 버튼 예제](uno-rev3-02/README.md)

___
.END OF README
