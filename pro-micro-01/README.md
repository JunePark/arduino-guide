# 아두이노 프로 마이크로 보드

* 보드 구입

ref) https://smartstore.naver.com/arduinostory/products/734177920?

ref) https://eduino.kr/product/detail.html?product_no=44

위의 링크에 보여준 보드는 pro micro 호환보드로써 저가의 중국산 보드입니다. 작동은 잘합니다.
하지만 가격을 낮추기 위해서 주로 USB 칩이 저가형을 사용하다보니 PC에서 드라이버를 잡아줘야해서 귀찮습니다.

아두이노는 생각보다 코딩보다는 하드웨어 구입이 훨씬 더 중요하다고 생각하는 1인입니다. 의외로 하드웨어 때문에 USB를 뺏다 꼽았다를 반복하는
경우가 많기 때문에 저렴한 호환보드를 사더라도 확실한 스펙이 적혀있는 물품은 사는것이 좋습니다. 달람 하드웨어 사진 하나 올려놓고 판매하는 곳
말고 제품의 스펙과 드라이버가 필요할시 드라이버 다운로드 링크를 적어넣고 예제까지 있는 곳이면 좋습니다.

추천할만한 사이트는

1. 자체 웹사이트가 있는 곳
 + 디바이스마트 : https://www.devicemart.co.kr/
 + 에듀이노: https://eduino.kr/
 + 엘라파츠: https://www.eleparts.co.kr/
 > NOTE. 엘레파츠는 아두이노 보다는 전자 부품 구입사이트로 더 유명함.

2. 네이버와 같은 곳에서 쇼핑몰을 운영하는 곳
 + 메이크잇펀: https://smartstore.naver.com/makeitfun
 + 코딩어레이: https://smartstore.naver.com/arduinostory
 > NOTE. https://cafe.naver.com/arduinostory 네이버 카페를 운영


그래서 뭐사라고?

1) 5V / 16MHz 에서 동작하는 ATMega32U4 제품인지 반드시 확인

2) 납땜하기 귀찮으면 납땜이 되어 있는 모델을 찾아서 구입

3) 디바이스 드라이버는 처음에는 바로 USB 연결하여 인식하면 별도로 설치하지 말고 인식이 안되면 제품 구입시 디바이스
링크가 있으면 그곳에서 다운로드 받아서 설치하고 링크가 없으면 sparkfun의 드라이버를 받아서 사용하면 됨.(아래의 링크 사이트 참조)

4) 저가형 보드를 사게 되면 보드 후면에 3.3V 5V 라고 프린트가 되어 있는걸 볼 수 있는데 정확하게는 제품의 사양에 따라
둘중의 하나만 프린트 되어야 하지만 저가형 보드를 구입하게 되면 그런게 없음. 구입후 마커로 표시를 해둘 필요가 있음.

5) 저가형 보드의 두번째 문제점은 LED의 색상이 green red 둘중에 random으로 발송될수 있습니다. 특히 power led 색상이
red가 발송될수 있으니 주의(사실 별 문제는 없지만 헷갈림)

6) 아두이노 프로 모델이 따로 있다. 핀 배열이 다르지만 아두이노 진품도 있고 sparkfun에서 만든 호환보드도 있다.
장점은 아두이노 프로 마이크로 보드와는 다르게 리셋 버튼이 보드에 있어서 편리하지만 프로 마이크로 보다는 살짝 큰게 단점 아닌 단점.


* 보드 설명

![pro-micro](./images/pro-micro-0000.png)

![pro-micro](./images/pro-micro-0001.png)

![pro-micro](./images/pro-micro-0002.png)

![pro-micro](./images/pro-micro-0003.png)

* blink 예제

ref) https://kdaa.tistory.com/31

ref) https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=acidc&logNo=221511986553

> NOTE. 참고로 arduino에서 Tools에서 보드를 선택하고 컴파일 한 다음 프로그램을 업로드하게 되는데 sparkfun의 pro micro 말고
arduino micro 와 같은 다른 보드를 선택하게 되면 프로그램이 돌아가는 것처럼 보일지 모르지만(blink 예제는 간단해서 실행될수 있음.)
사실 잘못한 것이고 이때 Windows의 장치관리자의 포트를 확인해보면 인식한 포트의 명칭이 바뀐것을 알수 있습니다. 그런 경우에는
제대로 된 보드를 선택하고 업로드하여 다시 기동하면 원래의 명칭대로 장치관리자에 인식한 것을 알 수 있습니다.

* 보드 개발 회사의 설명

pro micro는 정확하게 말하자면 arduino 와 sparkfun이 합작해서 만든 보드입니다.

핀 배열이나 상세한 설명은 아래의 링크를 참고하세요.

ref) https://learn.sparkfun.com/tutorials/pro-micro--fio-v3-hookup-guide/hardware-overview-pro-micro#:~:text=There%20are%20three%20LEDs%20on,data%20going%20out%20(TX).

* 보드가 벽돌된 경우

ref) https://sysdm.tistory.com/6


blink 예제
```
int RXLED = 17;  // D-17핀에 연결된 RX LED
// TX LED는 회로 구성이 되어 있지 않아 미리 정의된 매크로를 사용하여 제어합니다.
 
void setup(){
  pinMode(RXLED, OUTPUT);  // RX LED를 출력으로 설정합니다.
  // TX LED는 백그라운드에서 출력으로 설정되어 있습니다.
}
 
void loop(){
  digitalWrite(RXLED, LOW);   // RX LED 켜기
  TXLED0; // TX LED를 제어하는 매크로입니다. TX LED 끄기
  delay(1000);              // 1초간 유지합니다.
 
  digitalWrite(RXLED, HIGH);    // the RX LED 끄기
  TXLED1; // TX LED를 제어하는 매크로입니다. TX LED 켜기
  delay(1000);              // 1초간 유지합니다.
}
```


___
.END OF MAVEN-GOAL
