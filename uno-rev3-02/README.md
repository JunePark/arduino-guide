# 스위치 및 풀업 또는 풀다운 저항 이용 예제

* 택트 스위치를 이용한 LED on/off 예제

여기서는 풀다운 저항을 연결하여서 평상시에 LED off 이고 스위치를 클릭하여 onnd LED on

소스
```
int ledOut = 8;
int buttonIn = 9;

void setup() {
  pinMode(ledOut, OUTPUT);
  pinMode(buttonIn, INPUT);

}

void loop() {
  int buttonState = digitalRead(buttonIn);
  digitalWrite(ledOut, buttonState);
}
```

![uno-rev3-02-tact](./images/uno-rev3-02-tact-0000.png)

![uno-rev3-02-tact](./images/uno-rev3-02-tact-0000.mov)


cf) 풀다운 저항측 GND를 연결을 하지 않았을때 (floating 현상)

![uno-rev3-02-tact](./images/uno-rev3-02-tact-0001.mov)

> NOTE. 영상에서는 잘 포착되지 않았지만 풀다운 저항쪽의 GND를 제거하였을때 LED가 완전히 꺼진 상태는 아니고 LED의 밝기가 랜덤하게 변하는 상태임을 알수 있다. 


* 아날로그를 이용한 LED 맥북의 LED처럼 heartbeat

소스
```
int LED_pin = 3;

void setup() {
  pinMode(LED_pin, OUTPUT);
}

void loop() {
  int  i;

  for(i=0;i<256;i++) {
    analogWrite(LED_pin, i);
    delay(10);
  }
  for(i=255;i>=0;i--) {
    analogWrite(LED_pin, i);
    delay(10);
  }
}
```

![uno-rev3-02-heartbeat](./images/uno-rev3-02-heartbeat-0000.png)

![uno-rev3-02-heartbeat](./images/uno-rev3-02-heartbeat-0001.png)

![uno-rev3-02-heartbeat](./images/uno-rev3-02-heartbeat-0000.mov)


* 슬라이드 스위치를 이용한 LED on/off 예제

소스
```
void setup()
{
  Serial.begin(9600);
  
  pinMode(8, OUTPUT);
  pinMode(7, INPUT);
}

void loop()
{
  int readValue = digitalRead(7);
  Serial.println(readValue);
  
  if(readValue == HIGH) {
    digitalWrite(8, HIGH);
  }
  else {
    digitalWrite(8, LOW);
  }
}
```

![uno-rev3-03-slide](./images/uno-rev3-02-slide-0000.png)

![uno-rev3-03-slide](./images/uno-rev3-02-slide-0001.png)

![uno-rev3-04-slide](./images/uno-rev3-02-slide-0000.mov)


___
.END OF UNO_REV3
